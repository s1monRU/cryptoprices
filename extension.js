const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Soup = imports.gi.Soup;
const GLib = imports.gi.GLib;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;

// config
const wealth = [{
    ticker: 'BTC',
    amount: 0.8500001
}, {
    ticker: 'DOGE',
    amount: 6800.00
}, {
    ticker: 'LTC',
    amount: 12.89
}];

const tickers = ['BTC', 'ETH', 'BNB', 'DOGE', 'LTC'];
const coinMarketCapApiKey = '';


class Extension {
    constructor() {
        this._indicator = null;
    }

    rebuildPrices() {
        if (this.box) {
            this.box.destroy();
            this.box = undefined;
        }

        let soupSyncSession = new Soup.SessionSync();
        let url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=150&convert=USD';
        let message = Soup.Message.new('GET', url);
        message.request_headers.append('Accept', 'application/json');
        message.request_headers.append("X-CMC_PRO_API_KEY", coinMarketCapApiKey);
        message.request_headers.set_content_type("application/json", null);
        let responseCode = soupSyncSession.send_message(message);
        let res;

        this.box = new St.BoxLayout();

        if(responseCode === 200) {
            res = JSON.parse(message['response-body'].data);

            if (wealth.length > 0) {
                let wallet = 0;

                wealth.forEach((myCoin) => {
                    let coin = res.data.find((coin) => coin.symbol === myCoin.ticker);
                    let price = coin.quote.USD.price;

                    wallet += myCoin.amount * price;
                })

                let wealthLabel = new St.Label({
                    text: `$ ${wallet.toFixed(2)}     `,
                    y_align: Clutter.ActorAlign.CENTER,
                    style_class: 'white'
                });

                this.box.add_actor(wealthLabel);
            }

            tickers.forEach((ticker) => {
                let coin = res.data.find((coin) => coin.symbol === ticker);
                let price = coin.quote.USD.price;
                let priceLength = price.toFixed().length;
                let percentChange = coin.quote.USD.percent_change_24h;
                let isGrowing = percentChange >= 0;

                price = priceLength < 4 ? price.toFixed(3 - priceLength) : `${(price / 1000).toFixed(6 - priceLength)}k`;

                let coinPriceLabel = new St.Label({
                    text: `${ticker}: ${price}  `,
                    y_align: Clutter.ActorAlign.CENTER,
                    style_class: isGrowing ? 'green' : 'red'
                });

                this.box.add_actor(coinPriceLabel);
                this._indicator.add_actor(this.box);
            })
        }
    }
    
    enable() {
        const indicatorName = 'Cryptoprices';

        this._indicator = new PanelMenu.Button(0.0, indicatorName, false);

        this.rebuildPrices();
        this.timeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, (300000), () => {
            this.rebuildPrices();
            return GLib.SOURCE_CONTINUE;
        });


        Main.panel.addToStatusArea(indicatorName, this._indicator);
    }

    disable() {
        if (this.timeout) {
            GLib.Source.remove(this.timeout);
            this.timeout = null;
        }

        this._indicator.destroy();
        this._indicator = null;
    }
}


function init() {
    return new Extension();
}